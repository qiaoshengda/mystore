package com.bookstore.service;

import com.bookstore.entity.Category;
import com.bookstore.view.PageView;

import java.util.List;

/**
 * @author Administrator
 */
public interface CategoryService {

    /**
     * 获取全部分类
     *
     * @return 全部分类
     */
    List<Category> list();

    /**
     * 分页获取全部分类
     *
     * @param pageNum 页码
     * @param pageSize 页数
     * @return 分页
     */
    PageView<Category> page(Integer pageNum, Integer pageSize);

    /**
     * 新增分类
     *
     * @param name 分类名称
     * @return 新增结果
     */
    int add(String name);

    /**
     * 修改分类
     *
     * @param id 修改分类的ID
     * @param name 分类名称
     * @return 新增结果
     */
    int update(Integer id, String name);

    /**
     * 删除分类
     *
     * @param id 分类ID
     * @return 删除结果
     */
    int remove(Integer id);

}
