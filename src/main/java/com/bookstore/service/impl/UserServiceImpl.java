package com.bookstore.service.impl;

import cn.hutool.core.util.PageUtil;
import com.bookstore.dao.RecordDao;
import com.bookstore.dao.ShopDao;
import com.bookstore.dao.UserDao;
import com.bookstore.entity.Record;
import com.bookstore.entity.Shop;
import com.bookstore.entity.User;
import com.bookstore.option.RecordOption;
import com.bookstore.option.ShopOption;
import com.bookstore.option.UserOption;
import com.bookstore.service.UserService;
import com.bookstore.view.PageView;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Administrator
 */
@Service
public class UserServiceImpl implements UserService {

    private final UserDao userDao;
    private final ShopDao shopDao;
    private final RecordDao recordDao;

    public UserServiceImpl(UserDao userDao, ShopDao shopDao, RecordDao recordDao) {
        this.userDao = userDao;
        this.shopDao = shopDao;
        this.recordDao = recordDao;
    }

    @Override
    public User login(UserOption option) {
        return userDao.selectOne(option);
    }

    @Override
    public boolean register(User user) {
        // 查询用户名是否存在
        UserOption option = new UserOption();
        option.setUsername(user.getUsername());
        option.setIsDelete(0);
        List<User> users = userDao.selectBy(option);
        if (users.size() > 0){
            throw new  IllegalArgumentException("用户名已存在");
        }
        user.setIsDelete(0);
        user.setMoney(100);
        return userDao.insertSelective(user) > 0;
    }

    @Override
    public boolean update(User user) {
        return userDao.updateSelectiveById(user) > 0;
    }

    @Override
    public boolean logout(Integer id) {
        User user = new User();
        user.setId(id);
        user.setIsDelete(1);
        return userDao.updateSelectiveById(user) > 0;
    }

    @Override
    public User info(Integer id) {
        return userDao.selectById(id);
    }

    @Override
    public int enable(Integer id) {
        User user = userDao.selectById(id);
        if (user == null) {
            return -1;
        }
        User temp = new User();
        temp.setId(id);
        temp.setIsEnable(user.getIsEnable().equals(1) ? 0 : 1);
        return userDao.updateSelectiveById(temp);
    }

    @Override
    public PageView<User> page(Integer pageNum, Integer pageSize, UserOption option) {
        Long total = userDao.selectCountBy(option);
        if (total == 0) {
            return new PageView<>(pageNum, pageNum, 0, new LinkedList<>());
        }
        PageUtil.setFirstPageNo(1);
        option.setPageStart(PageUtil.getStart(pageNum, pageSize));
        option.setPageSize(pageSize);
        List<User> users = userDao.selectBy(option);
        return new PageView<>(pageNum, pageSize, total.intValue(), users);
    }

    @Override
    public PageView<Shop> shop(Integer id, Integer pageNum, Integer pageSize) {
        Long total = shopDao.selectCountBy(new ShopOption(id));
        if (total == 0) {
            return new PageView<>(pageNum, pageNum, 0, new LinkedList<>());
        }
        PageUtil.setFirstPageNo(1);
        List<Shop> shops = shopDao.selectBy(new ShopOption(id, PageUtil.getStart(pageNum, pageSize), pageSize));
        // 根据条件查询总数
        return new PageView<>(pageNum, pageSize, total.intValue(), shops);
    }

    @Override
    public PageView<Record> record(Integer id, Integer pageNum, Integer pageSize) {
        Long total = recordDao.selectCountBy(new RecordOption(id));
        if (total == 0) {
            return new PageView<>(pageNum, pageNum, 0, new LinkedList<>());
        }
        PageUtil.setFirstPageNo(1);
        List<Record> records = recordDao.selectBy(new RecordOption(id, PageUtil.getStart(pageNum, pageSize), pageSize));
        // 根据条件查询总数
        return new PageView<>(pageNum, pageSize, total.intValue(), records);
    }

}
