package com.bookstore.service.impl;

import cn.hutool.core.util.PageUtil;
import com.bookstore.dao.CategoryDao;
import com.bookstore.dao.CommodityDao;
import com.bookstore.entity.Category;
import com.bookstore.option.CategoryOption;
import com.bookstore.option.CommodityOption;
import com.bookstore.service.CategoryService;
import com.bookstore.view.PageView;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Administrator
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryDao categoryDao;
    private final CommodityDao commodityDao;

    public CategoryServiceImpl(CategoryDao categoryDao, CommodityDao commodityDao) {
        this.categoryDao = categoryDao;
        this.commodityDao = commodityDao;
    }

    @Override
    public List<Category> list() {
        return categoryDao.selectBy(new CategoryOption());
    }

    @Override
    public PageView<Category> page(Integer pageNum, Integer pageSize) {
        Long total = categoryDao.selectCountBy(new CategoryOption());
        if (total == 0) {
            return new PageView<>(pageNum, pageNum, 0, new LinkedList<>());
        }
        PageUtil.setFirstPageNo(1);
        List<Category> categories = categoryDao.selectBy(new CategoryOption(PageUtil.getStart(pageNum, pageSize), pageSize));
        return new PageView<>(pageNum, pageSize, total.intValue(), categories);
    }

    @Override
    public int add(String name) {
        Category category = new Category();
        category.setName(name);
        return categoryDao.insertSelective(category);
    }

    @Override
    public int update(Integer id, String name) {
        return categoryDao.updateSelectiveById(new Category(id, name));
    }

    @Override
    public int remove(Integer id) {
        // 判断分类下是否存在产品，存在需要将产品删除
        CommodityOption commodityOption = new CommodityOption();
        commodityOption.setCategoryId(id);
        commodityOption.setIsDelete(1);
        if (commodityDao.selectCountBy(commodityOption) > 0L) {
            return -1;
        }
        return categoryDao.deleteById(id);
    }


}
