package com.bookstore.service.impl;

import com.bookstore.dao.*;
import com.bookstore.entity.*;
import com.bookstore.option.CategoryOption;
import com.bookstore.option.CommodityOption;
import com.bookstore.option.ShopOption;
import com.bookstore.service.CommodityService;
import com.bookstore.view.CommodityView;
import com.bookstore.view.PageView;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Administrator
 */
@Service
public class CommodityServiceImpl implements CommodityService {

    private final CommodityDao commodityDao;
    private final CategoryDao categoryDao;
    private final ShopDao shopDao;
    private final UserDao userDao;
    private final RecordDao recordDao;

    public CommodityServiceImpl(CommodityDao commodityDao, CategoryDao categoryDao, ShopDao shopDao, UserDao userDao, RecordDao recordDao) {
        this.commodityDao = commodityDao;
        this.categoryDao = categoryDao;
        this.shopDao = shopDao;
        this.userDao = userDao;
        this.recordDao = recordDao;
    }

    @Override
    public PageView<CommodityView> page(Integer pageNum, Integer pageSize, CommodityOption option) {
        // 查询全部数量
        int total = commodityDao.selectCountBy(option).intValue();
        if (pageNum < 1 || pageSize < 1 || total < 1 || (pageNum - 1) * pageSize > total) {
            return new PageView<>();
        }
        option.setPageStart((pageNum - 1) * pageSize);
        if (total > (pageNum - 1) * pageSize && total < pageNum * pageSize){
            option.setPageSize(total - (pageNum - 1) * pageSize);
        }else {
            option.setPageSize(pageSize);
        }
        return new PageView<>(pageNum, pageSize, total, list(option));
    }

    @Override
    public List<CommodityView> list(CommodityOption option) {
        List<CommodityView> views = new LinkedList<>();
        option.setIsDelete(0);
        List<Commodity> commodities = commodityDao.selectBy(option);
        Map<Integer, Category> categoryMap = categoryDao.selectBy(new CategoryOption(commodities
                        .stream().map(Commodity::getCategoryId).distinct().collect(Collectors.toList())))
                .stream().collect(Collectors.toMap(Category::getId, e -> e));
        for (Commodity commodity : commodities) {
            CommodityView view = new CommodityView();
            BeanUtils.copyProperties(commodity, view);
            Category category = categoryMap.get(view.getCategoryId());
            if (category != null){
                view.setCategory(category.getName());
            }
            views.add(view);
        }
        return views;
    }

    @Override
    public CommodityView info(Integer id) {
        Commodity commodity = commodityDao.selectById(id);
        if (commodity == null || commodity.getIsDelete().equals(1)){
            return new CommodityView();
        }
        Category category = categoryDao.selectById(commodity.getCategoryId());
        CommodityView view = new CommodityView();
        BeanUtils.copyProperties(commodity, view);
        view.setCategory(category.getName());
        return view;
    }

    @Override
    public int add(Commodity commodity) {
        return commodityDao.insertSelective(commodity);
    }

    @Override
    public int update(Commodity commodity) {
        return commodityDao.updateSelectiveById(commodity);
    }

    @Override
    public int remove(Integer id) {
        Commodity commodity = new Commodity();
        commodity.setId(id);
        commodity.setIsDelete(1);
        return commodityDao.updateSelectiveById(commodity);
    }

    @Override
    public int enable(Integer id) {
        Commodity commodity = commodityDao.selectById(id);
        Commodity temp = new Commodity();
        temp.setId(commodity.getId());
        temp.setIsEnable(commodity.getIsEnable().equals(1) ? 0 : 1);
        return commodityDao.updateSelectiveById(temp);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean buy(Integer id, Integer userId) {
        //判断余额是否够
        User user = userDao.selectById(userId);
        int money = user.getMoney();
        Commodity commodity = commodityDao.selectById(id);
        if (commodity == null) {
            throw new IllegalArgumentException("商品不存在");
        }
        if (commodity.getIsDelete().equals(1)){
            throw new IllegalArgumentException("商品不存在");
        }
        if (commodity.getIsEnable().equals(0)){
            throw new IllegalArgumentException("商品已下架");
        }
        int commodityId = commodity.getId();
        int price = commodity.getPrice();
        int residue = money - price;
        if (residue > 0) {
            //1. 修改余额
            User record = new User();
            record.setId(userId);
            record.setMoney(residue);
            userDao.updateSelectiveById(record);
            //2. 删除购物车中的信息
            shopDao.deleteBy(new ShopOption(userId, commodityId));
            //3. 增加到记录表中
            recordDao.insertSelective(new Record(userId, commodityId, new Date()));
            return true;
        }
        return false;
    }

    @Override
    public void addToShop(Integer id, Integer userId) {
        shopDao.insertSelective(new Shop(userId, id));
    }

    @Override
    public void removeFromShop(Integer shopId, Integer userId) {
        ShopOption option = new ShopOption();
        option.setId(shopId);
        option.setUserId(userId);
        shopDao.deleteBy(option);
    }

}
