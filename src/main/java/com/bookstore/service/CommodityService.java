package com.bookstore.service;

import com.bookstore.entity.Commodity;
import com.bookstore.option.CommodityOption;
import com.bookstore.view.CommodityView;
import com.bookstore.view.PageView;

import java.util.List;

/**
 * @author Administrator
 */
public interface CommodityService {

    /**
     * 分页查询产品
     *
     * @param pageNum 页码
     * @param pageSize 每页大小
     * @param option 条件
     * @return 分页视图
     */
    PageView<CommodityView> page(Integer pageNum, Integer pageSize, CommodityOption option);

    /**
     * 根据条件查询产品
     *
     * @param option 查询条件
     * @return 符合条件的产品
     */
    List<CommodityView> list(CommodityOption option);

    /**
     * 根据ID查询产品
     *
     * @param id 产品ID
     * @return 产品
     */
    CommodityView info(Integer id);

    /**
     * 新增产品
     *
     * @param commodity 产品
     * @return 修改结果
     */
    int add(Commodity commodity);

    /**
     * 修改产品
     *
     * @param commodity 产品
     * @return 修改结果
     */
    int update(Commodity commodity);

    /**
     * 删除产品（禁止删除产品）
     *
     * @param id 产品ID
     * @return 删除结果
     */
    int remove(Integer id);

    /**
     * 启用/禁用产品
     *
     * @param id 产品ID
     * @return 结果
     */
    int enable(Integer id);

    /**
     * 购买一个产品
     *
     * @param id 产品ID
     * @param userId 用户ID
     * @return 操作结果
     */
    boolean buy(Integer id, Integer userId);

    /**
     * 增加到购物车
     *
     * @param id 产品ID
     * @param userId 用户ID
     */
    void addToShop(Integer id, Integer userId);

    /**
     * 从购物车中移除产品
     *
     * @param shopId 购物车ID
     * @param userId 用户ID
     */
    void removeFromShop(Integer shopId, Integer userId);

}
