package com.bookstore.service;

import com.bookstore.entity.Record;
import com.bookstore.entity.Shop;
import com.bookstore.entity.User;
import com.bookstore.option.UserOption;
import com.bookstore.view.PageView;

import java.util.List;

/**
 * @author Administrator
 */
public interface UserService {

    /**
     * 登录
     *
     * @param option 参数
     * @return 用户ID
     */
    User login(UserOption option);

    /**
     * 注册
     *
     * @param user 用户信息
     * @return 注册结果
     */
    boolean register(User user);

    /**
     * 更新用户
     *
     * @param user 用户信息
     * @return 更新结果
     */
    boolean update(User user);

    /**
     * 注销用户
     *
     * @param id 用户ID
     * @return 注销结果
     */
    boolean logout(Integer id);

    /**
     * 根据用户ID查询用户信息
     *
     * @param id 用户ID
     * @return 用户信息
     */
    User info(Integer id);

    /**
     * 启用/禁用用户
     *
     * @param id 用户ID
     * @return 操作结果
     */
    int enable(Integer id);

    /**
     * 分页查询用户
     *
     * @param pageNum 页码
     * @param pageSize 每页数目
     * @param option 条件
     * @return 结果
     */
    PageView<User> page(Integer pageNum, Integer pageSize, UserOption option);

    /**
     * 获取购物车列表
     *
     * @param id 用户ID
     * @param pageNum 页码
     * @param pageSize 每页大小
     * @return 购物车列表
     */
    PageView<Shop> shop(Integer id, Integer pageNum, Integer pageSize);

    /**
     * 获取购买记录列表
     *
     * @param id 用户ID
     * @param pageNum 页码
     * @param pageSize 每页大小
     * @return 购物车列表
     */
    PageView<Record> record(Integer id, Integer pageNum, Integer pageSize);

}
