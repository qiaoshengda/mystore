package com.bookstore.view;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Administrator
 */
public class PageView<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer pageNum;

    private Integer pageSize;

    private Integer count;

    private Integer total;

    private List<T> records;

    public PageView() {
        this.pageNum = 0;
        this.pageSize = 0;
        this.count = 0;
        this.total = 0;
        this.records = new LinkedList<>();
    }

    public PageView(Integer pageNum, Integer pageSize, Integer total, List<T> records) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.total = total;
        this.records = records;
        this.count = this.total / this.pageSize;
        if (this.total % this.pageSize > 0) {
            this.count += 1;
        }
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<T> getRecords() {
        return records;
    }

    public void setRecords(List<T> records) {
        this.records = records;
    }
}
