package com.bookstore.view;

/**
 * 结果集
 *
 * @author Administrator
 */
public class ResultVo<T> {

	/**
	 * 状态码
	 */
	private Integer code;

	/**
	 * 消息
	 */
	private String message;

	/**
	 * 数据
	 */
	private T data;
	public ResultVo(Integer code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public ResultVo(Integer code, String message, T data) {
		this.code = code;
		this.message = message;
		this.data = data;
	}

	public ResultVo() {
		super();
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
}
