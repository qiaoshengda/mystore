package com.bookstore.param;

import com.bookstore.entity.Commodity;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Administrator
 */
public class CommodityParam implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 图片名称
     */
    private String image;

    /**
     * 产品名称
     */
    private String name;

    /**
     * 分类
     */
    private Integer categoryId;

    /**
     * 价格
     */
    private Integer price;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Commodity toEntity(){
        Commodity commodity = new Commodity();
        commodity.setImage(this.image);
        commodity.setName(this.name);
        commodity.setCategoryId(this.categoryId);
        commodity.setPrice(this.price);
        commodity.setCreateTime(new Date());
        return commodity;
    }

    public Commodity toEntity(Integer id){
        Commodity commodity = toEntity();
        commodity.setId(id);
        commodity.setCreateTime(null);
        return commodity;
    }

}
