package com.bookstore.param;

import com.bookstore.option.UserOption;

import java.io.Serializable;

/**
 * @author Administrator
 */
public class LoginParam implements Serializable {

    private static final long serialVersionUID = 1L;

    private String username;

    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserOption toOption(){
        UserOption option = new UserOption();
        option.setUsername(this.username);
        option.setPassword(this.password);
        option.setIsDelete(0);
        return option;
    }

}
