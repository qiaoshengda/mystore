package com.bookstore.param;

import com.bookstore.entity.User;
import com.bookstore.option.UserOption;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Administrator
 */
public class UserParam implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 性别
     */
    private Boolean gender;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    public User toEntity(){
        User user = new User();
        user.setMoney(100);
        user.setUsername(this.username);
        user.setPassword(this.password);
        user.setAge(this.age);
        user.setGender(this.gender);
        user.setIsDelete(0);
        user.setCreateTime(new Date());
        return user;
    }

    public UserOption toOption(){
        UserOption option = new UserOption();
        option.setUsername(this.username);
        option.setPassword(this.password);
        option.setIsDelete(0);
        return option;
    }

}
