package com.bookstore.controller.client;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import com.bookstore.entity.*;
import com.bookstore.option.CommodityOption;
import com.bookstore.service.CategoryService;
import com.bookstore.service.CommodityService;
import com.bookstore.service.UserService;
import com.bookstore.view.CommodityView;
import com.bookstore.view.PageView;
import com.bookstore.view.RecordView;
import com.bookstore.view.ShopView;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Administrator
 */
@Controller("clientRouter")
public class RouterController {

    private final CommodityService commodityService;
    private final UserService userService;
    private final CategoryService categoryService;

    @RequestMapping("")
    public String welcome(
            Model model,
            @RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum,
            @RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize
    ) {
        return index(model, pageNum, pageSize);
    }

    @GetMapping("index.html")
    public String index(
            Model model,
            @RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum,
            @RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize
    ) {
        buildCommodityView(pageNum, pageSize, new CommodityOption(), model);
        List<Category> categories = categoryService.list();
        model.addAttribute("categories", categories);
        return "client/index";
    }

    /**
     * 个人信息页
     */
    @GetMapping("my.html")
    public String my(HttpServletRequest request, Model model) {
        Integer userId = (Integer) request.getSession().getAttribute("user");
        model.addAttribute("user", userService.info(userId));
        return "client/my";
    }

    /**
     * 购物车也
     */
    @GetMapping("shop.html")
    public String shop(
            HttpServletRequest request,
            Model model,
            @RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum,
            @RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize
    ) {
        Integer userId = (Integer) request.getSession().getAttribute("user");
        PageView<Shop> shops = userService.shop(userId, pageNum, pageSize);
        model.addAttribute("pageNum", shops.getPageNum());
        model.addAttribute("pageSize", shops.getPageSize());
        model.addAttribute("total", shops.getTotal());
        model.addAttribute("count", shops.getCount());
        if (shops.getTotal().equals(0)) {
            return "client/shop";
        }
        // 产品id集合
        List<Integer> ids = shops.getRecords().stream().map(Shop::getCommodityId).collect(Collectors.toCollection(LinkedList::new));
        // 获取产品
        CommodityOption option = new CommodityOption();
        option.setIds(ids.stream().distinct().collect(Collectors.toList()));
        Map<Integer, CommodityView> commodityMap = commodityService.list(option).stream().collect(Collectors.toMap(CommodityView::getId, e -> e));
        // 构建结果
        List<ShopView> shopViews = new LinkedList<>();
        for (Shop shop : shops.getRecords()) {
            ShopView shopView = new ShopView();
            CommodityView commodity = commodityMap.get(shop.getCommodityId());
            shopView.setId(shop.getId());
            if (commodity == null) {
                continue;
            }
            shopView.setCommodityId(commodity.getId());
            shopView.setName(commodity.getName());
            shopView.setImage(commodity.getImage());
            shopView.setCategoryId(commodity.getCategoryId());
            shopView.setCategory(commodity.getCategory());
            shopView.setPrice(commodity.getPrice());
            shopView.setIsEnable(commodity.getIsEnable());
            shopView.setCreateTime(commodity.getCreateTime());
            shopViews.add(shopView);
        }
        model.addAttribute("shops", shopViews);
        return "client/shop";
    }

    @GetMapping("record.html")
    public String record(
            HttpServletRequest request,
            Model model,
            @RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum,
            @RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize
    ) {
        Integer userId = (Integer) request.getSession().getAttribute("user");
        PageView<Record> records = userService.record(userId, pageNum, pageSize);
        model.addAttribute("pageNum", records.getPageNum());
        model.addAttribute("pageSize", records.getPageSize());
        model.addAttribute("total", records.getTotal());
        model.addAttribute("count", records.getCount());
        if (records.getTotal().equals(0)) {
            return "client/record";
        }
        CommodityOption option = new CommodityOption();
        List<Integer> ids = records.getRecords().stream().map(Record::getCommodityId).collect(Collectors.toCollection(LinkedList::new));
        option.setIds(ids.stream().distinct().collect(Collectors.toList()));
        Map<Integer, CommodityView> commodityViewMap = commodityService.list(option).stream().collect(Collectors.toMap(CommodityView::getId, e -> e));
        List<RecordView> recordViews = new LinkedList<>();
        for (Record record : records.getRecords()) {
            RecordView recordView = new RecordView();
            recordView.setId(record.getId());
            recordView.setCreateTime(record.getCreateTime());
            CommodityView commodityView = commodityViewMap.get(record.getCommodityId());
            if (commodityView == null) {
                continue;
            }
            recordView.setCommodityId(commodityView.getId());
            recordView.setCategoryId(commodityView.getCategoryId());
            recordView.setImage(commodityView.getImage());
            recordView.setName(commodityView.getName());
            recordView.setCategory(commodityView.getCategory());
            recordView.setPrice(commodityView.getPrice());
            recordView.setIsEnable(commodityView.getIsEnable());
            recordView.setCommodityCreateTime(commodityView.getCreateTime());
            recordViews.add(recordView);
        }
        model.addAttribute("records", recordViews);
        return "client/record";
    }

    @GetMapping("category.html")
    public String category(
            Model model,
            @RequestParam(value = "categoryId", required = false, defaultValue = "-1") Integer categoryId,
            @RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum,
            @RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize
    ) {
        List<Category> categories = categoryService.list();
        model.addAttribute("categoryId", categoryId);
        model.addAttribute("categories", categories);
        if (categoryId.equals(-1)) {
            categoryId = null;
        }
        CommodityOption option = new CommodityOption();
        option.setCategoryId(categoryId);
        buildCommodityView(pageNum, pageSize, option, model);
        return "client/category";
    }

    @GetMapping("search.html")
    public String search(
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "min", required = false, defaultValue = "0") Integer min,
            @RequestParam(value = "max", required = false, defaultValue = "0") Integer max,
            Model model,
            @RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum,
            @RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize
    ) {
        if (name == null || "".equals(name)) {
            model.addAttribute("commodities", null);
            return "client/search";
        }
        CommodityOption option = new CommodityOption();
        option.setName(name);
        option.setPriceMax(max);
        option.setPriceMin(min);
        buildCommodityView(pageNum, pageSize, option, model);
        model.addAttribute("name", name);
        model.addAttribute("max", max);
        model.addAttribute("min", min);
        return "client/search";
    }

    @RequestMapping("commodity/info/{id}.html")
    public String info(@PathVariable("id") int id, Model model) {
        CommodityView commodityView = commodityService.info(id);
        if (commodityView == null) {
            return "redirect:/";
        }
        model.addAttribute("commodity", commodityView);
        return "client/info";
    }

    @RequestMapping("commodity/pay.html")
    public String pay(int id, Model model, HttpServletRequest request) {
        Integer userId = (Integer) request.getSession().getAttribute("user");
        User user = userService.info(userId);
        if (userId != null) {
            CommodityView commodity = commodityService.info(id);
            if (commodity == null) {
                return "redirect:/";
            }
            model.addAttribute("user", user);
            model.addAttribute("commodity", commodity);
            return "client/pay";
        } else {
            return "redirect:/login.html";
        }
    }

    @GetMapping("login.html")
    public String loginView(
            @RequestParam(value = "message", required = false, defaultValue = "") String message,
            Model model
    ) {
        model.addAttribute("message", message);
        return "client/login";
    }

    @GetMapping("register.html")
    public String registerView(
            @RequestParam(value = "message", required = false, defaultValue = "") String message,
            Model model
    ) {
        model.addAttribute("message", message);
        return "client/register";
    }

    /**
     * 构建商品列表，并分页
     *
     * @param option 条件
     * @param model  model
     */
    private void buildCommodityView(Integer pageNum, Integer pageSize, CommodityOption option, Model model) {
        option.setIsEnable(1);
        PageView<CommodityView> page = commodityService.page(pageNum, pageSize, option);
        model.addAttribute("pageNum", page.getPageNum());
        model.addAttribute("pageSize", page.getPageSize());
        model.addAttribute("total", page.getTotal());
        model.addAttribute("count", page.getCount());
        model.addAttribute("commodities", page.getRecords());
    }

    public RouterController(CommodityService commodityService, UserService userService, CategoryService categoryService) {
        this.commodityService = commodityService;
        this.userService = userService;
        this.categoryService = categoryService;
    }
}
