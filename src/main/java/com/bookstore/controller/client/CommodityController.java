package com.bookstore.controller.client;

import javax.servlet.http.HttpServletRequest;

import com.bookstore.dao.*;
import com.bookstore.entity.*;
import com.bookstore.option.ShopOption;
import com.bookstore.service.CommodityService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bookstore.view.ResultVo;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Administrator
 */
@RestController("clientCommodity")
@RequestMapping("commodity")
public class CommodityController {
	
	private final CommodityService commodityService;

	public static final String USER = "user";

	public CommodityController(CommodityService commodityService) {
		this.commodityService = commodityService;
	}

	@RequestMapping("addShop")
	public ResultVo<Void> addShop(Integer id, HttpServletRequest request) {
		Integer userId = (Integer) request.getSession().getAttribute("user");
		commodityService.addToShop(id, userId);
		return new ResultVo<>(200, "加入购物车成功");
	}

	@GetMapping("removeShop")
	public ResultVo<Void> removeShop(Integer id, HttpServletRequest request) {
		Integer userId = (Integer) request.getSession().getAttribute("user");
		commodityService.removeFromShop(id, userId);
		return new ResultVo<>(200, "移除购物车成功");
	}
	
	@RequestMapping("buy")
	public ResultVo<Void> buy(int id, HttpServletRequest request) {
		if (commodityService.buy(id, (Integer) request.getSession().getAttribute(USER))) {
			return new ResultVo<>(200, "购买成功");
		}else {
			return new ResultVo<>(400, "购买失败，余额不足");
		}
	}

}
