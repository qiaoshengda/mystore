package com.bookstore.controller.client;

import javax.servlet.http.HttpServletRequest;

import com.bookstore.param.LoginParam;
import com.bookstore.param.UserParam;
import com.bookstore.service.UserService;
import org.springframework.web.bind.annotation.*;

import com.bookstore.entity.User;
import com.bookstore.view.ResultVo;

/**
 * @author Administrator
 */
@RestController("clientUser")
@RequestMapping("user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("login")
    public ResultVo<Void> login(LoginParam param, HttpServletRequest request) {
        User temp = userService.login(param.toOption());
        if (temp != null) {
            if (temp.getIsEnable() == 0) {
                // 用户未启用，请联系管理员启用
                return new ResultVo<>(500, "用户已被禁用，请联系管理员启用");
            }
            request.getSession().setAttribute("user", temp.getId());
            request.getSession().setAttribute("username", temp.getUsername());
            return new ResultVo<>(200, "登录成功");
        } else {
            // 用户名或密码错误
            return new ResultVo<>(500, "用户名或密码错误");
        }
    }

    @RequestMapping("register")
    public ResultVo<Void> register(UserParam param, HttpServletRequest request) {
        userService.register(param.toEntity());
        User temp = userService.login(param.toOption());
        request.getSession().setAttribute("user", temp.getId());
        request.getSession().setAttribute("username", temp.getUsername());
        return new ResultVo<>(200, "注册成功");

    }

    @RequestMapping("exit")
    public ResultVo<Void> exit(HttpServletRequest request) {
        request.getSession().removeAttribute("user");
        return new ResultVo<>(200, "退出成功");
    }

    @PostMapping("update")
    public ResultVo<Void> update(@RequestBody UserParam param, HttpServletRequest request) {
        Integer userId = (Integer) request.getSession().getAttribute("user");
        User user = param.toEntity();
        user.setId(userId);
        if (userService.update(user)) {
            request.getSession().removeAttribute("user");
            return new ResultVo<>(200, "修改成功");
        }
        return new ResultVo<>(400, "修改失败");
    }

    @GetMapping("logout")
    public ResultVo<Void> layout(HttpServletRequest request) {
        Integer userId = (Integer) request.getSession().getAttribute("user");
        if (userService.logout(userId)) {
            request.getSession().removeAttribute("user");
            return new ResultVo<>(200, "注销成功");
        }
        return new ResultVo<>(200, "注销失败");
    }

}
