package com.bookstore.controller.client;

import com.bookstore.view.ResultVo;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author Administrator
 */
@RestControllerAdvice
public class ExceptionController {

    @ExceptionHandler(IllegalArgumentException.class)
    public ResultVo<Void> illegalArgumentException(IllegalArgumentException e){
        return new ResultVo<>(500, e.getMessage());
    }

}
