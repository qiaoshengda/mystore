package com.bookstore.controller.admin;

import com.bookstore.dao.AdminDao;
import com.bookstore.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Administrator
 */
@Controller
@RequestMapping("admin")
public class AdminController {

    private final AdminDao adminDao;

    public AdminController(AdminDao adminDao) {
        this.adminDao = adminDao;
    }

    @RequestMapping("login")
    public String login(User user, HttpServletRequest request) {
        Integer login = adminDao.login(user);
        if (login != null) {
            request.getSession().setAttribute("admin", user);
            return "redirect:/admin/";
        }
        return "redirect:/admin/login.html?message=1";
    }

    @RequestMapping("exit")
    public String exit(HttpServletRequest request) {
        request.getSession().removeAttribute("admin");
        return "redirect:/admin/login.html";
    }

}
