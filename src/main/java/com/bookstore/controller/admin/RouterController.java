package com.bookstore.controller.admin;

import java.util.List;

import com.bookstore.entity.User;
import com.bookstore.option.CommodityOption;
import com.bookstore.option.UserOption;
import com.bookstore.service.CategoryService;
import com.bookstore.service.CommodityService;
import com.bookstore.service.UserService;
import com.bookstore.view.CommodityView;
import com.bookstore.view.PageView;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bookstore.entity.Category;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Administrator
 */
@Controller("adminRouter")
@RequestMapping("admin")
public class RouterController {
	
	private final UserService userService;
	private final CommodityService commodityService;
	private final CategoryService categoryService;

	public RouterController(UserService userService, CommodityService commodityService, CategoryService categoryService) {
		this.userService = userService;
		this.commodityService = commodityService;
		this.categoryService = categoryService;
	}

	@RequestMapping("")
	public String welcome() {
		return "admin/index";
	}

	@RequestMapping("index.html")
	public String index() {
		return "admin/index";
	}
	
	@RequestMapping("user.html")
	public String user(
			Model model,
			@RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize
	) {
		UserOption userOption = new UserOption();
		userOption.setIsDelete(0);
		PageView<User> page = userService.page(pageNum, pageSize, userOption);
		model.addAttribute("pageNum", page.getPageNum());
		model.addAttribute("pageSize", page.getPageSize());
		model.addAttribute("total", page.getTotal());
		model.addAttribute("count", page.getCount());
		model.addAttribute("users", page.getRecords());
		return "admin/user";
	}
	
	@RequestMapping("commodity.html")
	public String commodity(
			Model model,
			@RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize
	) {
		PageView<CommodityView> page = commodityService.page(pageNum, pageSize, new CommodityOption());
		model.addAttribute("pageNum", page.getPageNum());
		model.addAttribute("pageSize", page.getPageSize());
		model.addAttribute("total", page.getTotal());
		model.addAttribute("count", page.getCount());
		model.addAttribute("commodities", page.getRecords());
		List<Category> categories = categoryService.list();
		model.addAttribute("categories", categories);
		return "admin/commodity";
	}
	
	@GetMapping("login.html")
	public String loginView(){
		return "admin/login";
	}

	@GetMapping("category.html")
	public String category(
			Model model,
			@RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize
	) {
		PageView<Category> page = categoryService.page(pageNum, pageSize);
		model.addAttribute("pageNum", page.getPageNum());
		model.addAttribute("pageSize", page.getPageSize());
		model.addAttribute("total", page.getTotal());
		model.addAttribute("count", page.getCount());
		model.addAttribute("categories", page.getRecords());
		return "admin/category";
	}


}
