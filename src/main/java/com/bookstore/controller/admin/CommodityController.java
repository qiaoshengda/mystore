package com.bookstore.controller.admin;

import com.bookstore.entity.Category;
import com.bookstore.entity.Commodity;
import com.bookstore.param.CommodityParam;
import com.bookstore.service.CategoryService;
import com.bookstore.service.CommodityService;
import com.bookstore.view.CommodityView;
import com.bookstore.view.ResultVo;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author Administrator
 */
@RestController("adminCommodity")
@RequestMapping("admin/commodity")
public class CommodityController {

    private final CommodityService commodityService;
    private final CategoryService categoryService;

    public CommodityController(CommodityService commodityService, CategoryService categoryService) {
        this.commodityService = commodityService;
        this.categoryService = categoryService;
    }

    @GetMapping("info/{id}")
    public ResultVo<CommodityView> info(@PathVariable("id") Integer id) {
        return new ResultVo<>(200, "success", commodityService.info(id));
    }

    @GetMapping("category")
    public ResultVo<List<Category>> category() {
        return new ResultVo<>(200, "success", categoryService.list());
    }

    @RequestMapping("enable/{id}")
    public ResultVo<Void> enable(@PathVariable("id") int id) {
        commodityService.enable(id);
        return new ResultVo<>(200, "success");
    }

    @PostMapping("add")
    public ResultVo<Void> add(@RequestBody CommodityParam param) {
        commodityService.add(param.toEntity());
        return new ResultVo<>(200, "success");
    }

    @PostMapping("update/{id}")
    public ResultVo<Void> update(
            @PathVariable("id") Integer id,
            @RequestBody CommodityParam param
    ) {
        commodityService.update(param.toEntity(id));
        return new ResultVo<>(200, "success");
    }

    @PostMapping("upload")
    public ResultVo<String> commodityUpload(MultipartFile multipartFile, HttpServletRequest request) throws IOException {
        String name = multipartFile.getOriginalFilename();
        if (name == null || "".equals(name)) {
            name = System.currentTimeMillis() + "";
        }
        String realPath = request.getRealPath("/");
        File file = new File(realPath, name);
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        InputStream inputStream = multipartFile.getInputStream();
        byte[] b = new byte[1024];
        while (inputStream.read(b) != -1) {
            fileOutputStream.write(b);
        }
        ResultVo<String> success = new ResultVo<>(200, "success");
        success.setData(name);
        return success;
    }

}
