package com.bookstore.controller.admin;

import com.bookstore.entity.Category;
import com.bookstore.service.CategoryService;
import com.bookstore.view.ResultVo;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Administrator
 */
@RestController
@RequestMapping("admin/category")
public class CategoryController {

    private final CategoryService categoryService;

    @GetMapping("list")
    public ResultVo<List<Category>> list() {
        return new ResultVo<>(200, "success", categoryService.list());
    }

    @GetMapping("add")
    public ResultVo<Void> add(String name) {
        categoryService.add(name);
        return new ResultVo<>(200, "success");
    }

    @PostMapping("update/{id}")
    public ResultVo<Void> update(@PathVariable Integer id, @RequestBody Category category) {
        categoryService.update(id, category.getName());
        return new ResultVo<>(200, "success");
    }

    @GetMapping("remove/{id}")
    public ResultVo<Void> remove(@PathVariable Integer id) {
        int remove = categoryService.remove(id);
        if (remove == -1) {
            return new ResultVo<>(500, "分类下存在产品，请删除产品后删除分类");
        }
        return new ResultVo<>(200, "success");
    }

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }
}
