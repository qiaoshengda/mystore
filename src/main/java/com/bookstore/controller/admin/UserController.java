package com.bookstore.controller.admin;

import com.bookstore.service.UserService;
import com.bookstore.view.ResultVo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Administrator
 */
@RestController("adminUser")
@RequestMapping("admin/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("remove/{id}")
    public ResultVo<Void> remove(@PathVariable("id") Integer id) {
        if (userService.logout(id)) {
            return new ResultVo<>(200, "success");
        }
        return new ResultVo<>(400, "fail");
    }

    @GetMapping("enable/{id}")
    public ResultVo<Void> enable(@PathVariable("id") Integer id) {
        int enable = userService.enable(id);
        if (enable == -1) {
            return new ResultVo<>(500, "用户不存在");
        }else if (enable == 0) {
            return new ResultVo<>(400, "fail");
        }
        return new ResultVo<>(200, "success");
    }

}
