package com.bookstore.filter;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.json.JSONUtil;
import com.bookstore.view.ResultVo;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * 认证拦截器
 *
 * @author Administrator
 */
@WebFilter(filterName = "AuthenticationFilter", urlPatterns = "/*")
public class AuthenticationFilter implements Filter {

    /**
     * 管理员页面前缀
     */
    private static final String ADMIN_PREFIX = "/admin";

    /**
     * 页面后缀
     */
    private static final String PATH_SUFFIX = ".html";

    /**
     * 管理员忽略登录页面
     */
    private static final String ADMIN_LOGIN_HTML = "/admin/login.html";

    /**
     * 管理员忽略登录操作
     */
    private static final String ADMIN_LOGIN_OPERATION = "/admin/login";

    /**
     * 管理员凭证标识
     */
    private static final String ADMIN_TOKEN = "admin";

    /**
     * 用户凭证标识
     */
    private static final String CLIENT_TOKEN = "user";

    private static final List<String> CLIENT_NEED_HTML = CollUtil.newLinkedList("/my.html", "/pay.html", "/record.html", "/shop.html");

    private static final List<String> CLIENT_NEED_OPERATION = CollUtil.newLinkedList(
            "/commodity/addShop",
            "/commodity/removeShop",
            "/commodity/buy",
            "/user/exit",
            "/user/update",
            "/user/layout");

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String servletPath = request.getServletPath();
        if (servletPath.startsWith(ADMIN_PREFIX)) {
            if (servletPath.endsWith(PATH_SUFFIX)) {
                // 如果是管理员页面，则调用管理员拦截器
                adminRouteFilter(servletPath, request, response, filterChain);
            }else {
                adminApiFilter(servletPath, request, response, filterChain);
            }
        }else {
            if (servletPath.endsWith(PATH_SUFFIX)) {
                clientRouteFilter(servletPath, request, response, filterChain);
            }else {
                clientApiFilter(servletPath, request, response, filterChain);
            }
        }
    }

    @Override
    public void destroy() {

    }

    /**
     * 管理员页面拦截器
     */
    private void adminRouteFilter(String servletPath, HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (servletPath.equals(ADMIN_LOGIN_HTML)) {
            // 登录页取消拦截
            filterChain.doFilter(request, response);
        }else {
            HttpSession session = request.getSession(false);
            if (session != null && session.getAttribute(ADMIN_TOKEN) != null) {
                filterChain.doFilter(request, response);
            }else {
                response.sendRedirect(request.getContextPath() + "/admin/login.html");
            }
        }
    }

    /**
     * 管理员接口拦截器
     */
    private void adminApiFilter(String servletPath, HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (servletPath.equals(ADMIN_LOGIN_OPERATION)) {
            // 登录页取消拦截
            filterChain.doFilter(request, response);
        }else {
            HttpSession session = request.getSession(false);
            if (session != null && session.getAttribute(ADMIN_TOKEN) != null) {
                filterChain.doFilter(request, response);
            }else {
                response.setContentType("application/json");
                response.setCharacterEncoding("utf-8");
                response.getWriter().println(JSONUtil.toJsonStr(new ResultVo<Void>(401, "未认证")));
            }
        }
    }

    /**
     * 客户端页面拦截器
     */
    private void clientRouteFilter(String servletPath, HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (CLIENT_NEED_HTML.contains(servletPath)) {
            HttpSession session = request.getSession(false);
            if (session != null && session.getAttribute(CLIENT_TOKEN) != null) {
                filterChain.doFilter(request, response);
            }else {
                response.sendRedirect(request.getContextPath() + "/login.html");
            }
        }else {
            filterChain.doFilter(request, response);
        }
    }

    /**
     * 客户端接口拦截器
     */
    private void clientApiFilter(String servletPath, HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (CLIENT_NEED_OPERATION.contains(servletPath)) {
            HttpSession session = request.getSession(false);
            if (session != null && session.getAttribute(CLIENT_TOKEN) != null) {
                filterChain.doFilter(request, response);
            }else {
                response.setContentType("application/json");
                response.setCharacterEncoding("utf-8");
                response.getWriter().println(JSONUtil.toJsonStr(new ResultVo<Void>(401, "未认证")));
            }
        }else {
            filterChain.doFilter(request, response);
        }
    }

}
