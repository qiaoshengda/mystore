package com.bookstore.entity;

/**
 * 分类表
 *
 * @author Administrator
 */
public class Category {

	/**
	 * 分类ID
	 */
	private Integer id;
	/**
	 * 分类名
	 */
	private String name;

	public Category() {
	}

	public Category(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
