package com.bookstore.entity;

/**
 * 购物车
 *
 * @author Administrator
 */
public class Shop {

	/**
	 * 购物车ID
	 */
	private int id;

	/**
	 * 用户ID
	 */
	private int userId;

	/**
	 * 产品ID
	 */
	private int commodityId;
	public Shop(int userId, int commodityId) {
		super();
		this.userId = userId;
		this.commodityId = commodityId;
	}
	public Shop() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getCommodityId() {
		return commodityId;
	}
	public void setCommodityId(int commodityId) {
		this.commodityId = commodityId;
	}
	
	
}
