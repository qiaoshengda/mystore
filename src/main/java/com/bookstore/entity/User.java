package com.bookstore.entity;

import java.util.Date;

/**
 * 用户
 *
 * @author Administrator
 */
public class User {

	public User() {
	}

	public User(String username, String password) {
		this.username = username;
		this.password = password;
	}

	/**
	 * 用户ID
	 */
	private Integer id;

	/**
	 * 用户名
	 */
	private String username;

	/**
	 * 密码
	 */
	private String password;

	/**
	 * 年龄
	 */
	private Integer age;

	/**
	 * 性别
	 */
	private Boolean gender;

	/**
	 * 价格
	 */
	private Integer money;

	/**
	 * 是否启用
	 */
	private Integer isEnable;

	/**
	 * 逻辑删除
	 */
	private Integer isDelete;

	/**
	 * 创建时间
	 */
	private Date createTime;

	public Integer getMoney() {
		return money;
	}
	public void setMoney(Integer money) {
		this.money = money;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Boolean getGender() {
		return gender;
	}
	public void setGender(Boolean gender) {
		this.gender = gender;
	}

	public Integer getIsDelete() {
		return isDelete;
	}

	public Integer getIsEnable() {
		return isEnable;
	}

	public void setIsEnable(Integer isEnable) {
		this.isEnable = isEnable;
	}

	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}
