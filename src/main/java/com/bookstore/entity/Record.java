package com.bookstore.entity;


import java.util.Date;

/**
 * 购物记录
 *
 * @author Administrator
 */
public class Record {

	/**
	 * 购物记录ID
	 */
	private Integer id;

	/**
	 * 用户ID
	 */
	private Integer userId;

	/**
	 * 产品ID
	 */
	private Integer commodityId;

	/**
	 * 创建时间
	 */
	private Date createTime;

	public Record() {
	}

	public Record(Integer userId, Integer commodityId, Date createTime) {
		this.userId = userId;
		this.commodityId = commodityId;
		this.createTime = createTime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getCommodityId() {
		return commodityId;
	}

	public void setCommodityId(Integer commodityId) {
		this.commodityId = commodityId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}
