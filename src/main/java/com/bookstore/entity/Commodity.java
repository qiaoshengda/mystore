package com.bookstore.entity;

import java.util.Date;

/**
 * 产品
 * @author Administrator
 */
public class Commodity {

	/**
	 * 产品ID
	 */
	private Integer id;

	/**
	 * 图片名称
	 */
	private String image;

	/**
	 * 产品名称
	 */
	private String name;

	/**
	 * 分类
	 */
	private Integer categoryId;

	/**
	 * 价格
	 */
	private Integer price;

	/**
	 * 是否启用
	 */
	private Integer isEnable;

	/**
	 * 逻辑删除
	 */
	private Integer isDelete;

	/**
	 * 创建时间
	 */
	private Date createTime;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getIsEnable() {
		return isEnable;
	}

	public void setIsEnable(Integer isEnable) {
		this.isEnable = isEnable;
	}

	public Integer getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}
