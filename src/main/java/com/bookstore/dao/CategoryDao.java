package com.bookstore.dao;

import java.util.List;

import com.bookstore.entity.Category;
import com.bookstore.option.CategoryOption;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Administrator
 */
@Mapper
public interface CategoryDao {

	/**
	 * 查询分类个数
	 *
	 * @param option 查询条件
	 * @return 全部分类
	 */
	Long selectCountBy(CategoryOption option);

	/**
	 * 查询全部分类
	 *
	 * @param option 查询条件
	 * @return 全部分类
	 */
	List<Category> selectBy(CategoryOption option);

	/**
	 * 根据ID查询分类
	 *
	 * @param id 分类ID
	 * @return 分类
	 */
	Category selectById(Integer id);

	/**
	 * 新增分类
	 *
	 * @param category 分类
	 * @return 结果
	 */
	int insertSelective(Category category);

	/**
	 * 更新分类
	 *
	 * @param category 分类
	 * @return 结果
	 */
	int updateSelectiveById(Category category);

	/**
	 * 通过ID删除分类
	 *
	 * @param id 分类ID
	 * @return 删除结果
	 */
	int deleteById(Integer id);

}
