package com.bookstore.dao;

import com.bookstore.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Administrator
 */
@Mapper
public interface AdminDao {

	/**
	 * 登录
	 *
	 * @param user 用户信息
	 * @return 查询结果
	 */
	Integer login(User user);

}
