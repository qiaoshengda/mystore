package com.bookstore.dao;

import java.util.List;

import com.bookstore.option.RecordOption;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.bookstore.entity.Commodity;
import com.bookstore.entity.Record;

/**
 * @author Administrator
 */
@Mapper
public interface RecordDao {

	/**
	 * 根据条件查询总数
	 * @param option 查询条件
	 * @return 总数
	 */
	Long selectCountBy(RecordOption option);

	/**
	 * 根据条件查询购买记录
	 *
	 * @param option 查询条件
	 * @return 全部购买记录
	 */
	List<Record> selectBy(RecordOption option);

	/**
	 * 新增购买记录
	 *
	 * @param record 购买记录
	 * @return 新增结果
	 */
	int insertSelective(Record record);

	/**
	 * 根据条件更新购买记录
	 *
	 * @param record 条件
	 * @return 删除结果
	 */
	int updateSelectiveById(Record record);

}
