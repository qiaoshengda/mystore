package com.bookstore.dao;

import java.util.List;

import com.bookstore.entity.Shop;
import com.bookstore.option.ShopOption;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Administrator
 */
@Mapper
public interface ShopDao {

	/**
	 * 根据条件查询购物车总数
	 * @param option 查询条件
	 * @return 全部购买记录总数
	 */
	Long selectCountBy(ShopOption option);

	/**
	 * 根据条件查询购物车
	 *
	 * @param option 查询条件
	 * @return 全部购买记录
	 */
	List<Shop> selectBy(ShopOption option);

	/**
	 * 新增购物车
	 *
	 * @param record 购买记录
	 * @return 新增结果
	 */
	int insertSelective(Shop record);

	/**
	 * 根据条件删除购物车
	 *
	 * @param option 用户ID
	 * @return 删除结果
	 */
	int deleteBy(ShopOption option);


}
