package com.bookstore.dao;

import java.util.List;

import com.bookstore.option.CommodityOption;
import org.apache.ibatis.annotations.Mapper;

import com.bookstore.entity.Commodity;

/**
 * @author Administrator
 */
@Mapper
public interface CommodityDao {

	/**
	 * 查询全部产品
	 *
	 * @param option 查询条件
	 * @return 全部产品
	 */
	List<Commodity> selectBy(CommodityOption option);

	/**
	 * 查询全部产品个数
	 *
	 * @param option 查询条件
	 * @return 全部产品
	 */
	Long selectCountBy(CommodityOption option);

	/**
	 * 根据ID查询产品
	 * @param id 产品ID
	 * @return 产品信息
	 */
	Commodity selectById(int id);

	/**
	 * 新增产品
	 *
	 * @param commodity 产品信息
	 * @return 新增结果
	 */
	int insertSelective(Commodity commodity);

	/**
	 * 更新产品数据通过ID
	 *
	 * @param commodity 产品
	 * @return
	 */
	int updateSelectiveById(Commodity commodity);

}
