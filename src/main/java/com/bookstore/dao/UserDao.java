package com.bookstore.dao;

import java.util.List;

import com.bookstore.entity.Category;
import com.bookstore.option.CategoryOption;
import com.bookstore.option.UserOption;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.bookstore.entity.User;

/**
 * @author Administrator
 */
@Mapper
public interface UserDao {

	/**
	 * 查询全部用户数量
	 *
	 * @param option 查询条件
	 * @return 全部用户数量
	 */
	Long selectCountBy(UserOption option);

	/**
	 * 查询全部用户
	 *
	 * @param option 查询条件
	 * @return 全部用户
	 */
	List<User> selectBy(UserOption option);

	/**
	 * 查询一个用户
	 *
	 * @param option 查询条件
	 * @return 全部用户
	 */
	User selectOne(UserOption option);

	/**
	 * 根据ID查询用户
	 *
	 * @param id 用户ID
	 * @return 用户
	 */
	User selectById(Integer id);

	/**
	 * 新增用户
	 *
	 * @param user 用户
	 * @return 新增结果
	 */
	int insertSelective(User user);

	/**
	 * 修改用户
	 *
	 * @param user 用户
	 * @return 更新结果
	 */
	int updateSelectiveById(User user);

}
