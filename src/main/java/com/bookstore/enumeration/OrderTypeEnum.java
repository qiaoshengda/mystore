package com.bookstore.enumeration;

/**
 * 排序类型
 *
 * @author Administrator
 */
public enum OrderTypeEnum {

    // 不排序
    NO_ORDER(0, ""),
    // 正序，从小到大
    ASC_ORDER(1, "asc"),
    // 倒叙，从大到小
    DESC_ORDER(2, "desc");

    private Integer value;
    private String name;

    OrderTypeEnum(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static OrderTypeEnum getByValue(Integer value){
        for (OrderTypeEnum orderTypeEnum : values()) {
            if (orderTypeEnum.getValue().equals(value)){
                return orderTypeEnum;
            }
        }
        return null;
    }

}
