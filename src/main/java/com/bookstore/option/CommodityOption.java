package com.bookstore.option;

import java.io.Serializable;
import java.util.List;

/**
 * @author Administrator
 */
public class CommodityOption implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * IDs
     */
    private List<Integer> ids;

    /**
     * 分类名称查询
     */
    private Integer categoryId;

    /**
     * 产品名称
     */
    private String name;

    /**
     * 价格范围开始
     */
    private Integer priceMax;

    /**
     * 价格范围截止
     */
    private Integer priceMin;

    /**
     * 根据创建时间排序
     */
    private String orderByCreateTime;

    /**
     * 根据价格排序
     */
    private String orderByPrice;

    /**
     * 开始数量
     */
    private Integer pageStart;

    /**
     * 每页大小
     */
    private Integer pageSize;

    /**
     * 是否启用
     */
    private Integer isEnable;

    /**
     * 逻辑删除
     */
    private Integer isDelete;

    public CommodityOption() {
    }

    public CommodityOption(Integer isEnable) {
        this.isEnable = isEnable;
    }

    public List<Integer> getIds() {
        return ids;
    }

    public void setIds(List<Integer> ids) {
        this.ids = ids;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPriceMax() {
        return priceMax;
    }

    public void setPriceMax(Integer priceMax) {
        this.priceMax = priceMax;
    }

    public Integer getPriceMin() {
        return priceMin;
    }

    public void setPriceMin(Integer priceMin) {
        this.priceMin = priceMin;
    }

    public String getOrderByCreateTime() {
        return orderByCreateTime;
    }

    public void setOrderByCreateTime(String orderByCreateTime) {
        this.orderByCreateTime = orderByCreateTime;
    }

    public String getOrderByPrice() {
        return orderByPrice;
    }

    public void setOrderByPrice(String orderByPrice) {
        this.orderByPrice = orderByPrice;
    }

    public Integer getPageStart() {
        return pageStart;
    }

    public void setPageStart(Integer pageStart) {
        this.pageStart = pageStart;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(Integer isEnable) {
        this.isEnable = isEnable;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }
}
