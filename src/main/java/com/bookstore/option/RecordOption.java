package com.bookstore.option;

import java.io.Serializable;

/**
 * @author Administrator
 */
public class RecordOption implements Serializable {

    private static final long serialVersionUID = 1L;

    public RecordOption() {
    }

    public RecordOption(Integer userId) {
        this.userId = userId;
    }

    public RecordOption(Integer userId, Integer pageStart, Integer pageSize) {
        this.userId = userId;
        this.pageStart = pageStart;
        this.pageSize = pageSize;
    }

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 产品ID
     */
    private Integer commodityId;

    private Integer pageStart;

    private Integer pageSize;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCommodityId() {
        return commodityId;
    }

    public void setCommodityId(Integer commodityId) {
        this.commodityId = commodityId;
    }

    public Integer getPageStart() {
        return pageStart;
    }

    public void setPageStart(Integer pageStart) {
        this.pageStart = pageStart;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
