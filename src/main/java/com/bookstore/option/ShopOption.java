package com.bookstore.option;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Administrator
 */
public class ShopOption implements Serializable {

    private static final long serialVersionUID = 1L;

    public ShopOption() {
    }

    public ShopOption(Integer userId) {
        this.userId = userId;
    }

    public ShopOption(Integer userId, Integer commodityId) {
        this.userId = userId;
        this.commodityId = commodityId;
    }

    public ShopOption(Integer userId, Integer pageStart, Integer pageSize) {
        this.userId = userId;
        this.pageStart = pageStart;
        this.pageSize = pageSize;
    }

    /**
     * 购物车ID
     */
    private Integer id;

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 产品ID
     */
    private Integer commodityId;

    private Integer pageStart;

    private Integer pageSize;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCommodityId() {
        return commodityId;
    }

    public void setCommodityId(Integer commodityId) {
        this.commodityId = commodityId;
    }

    public Integer getPageStart() {
        return pageStart;
    }

    public void setPageStart(Integer pageStart) {
        this.pageStart = pageStart;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}