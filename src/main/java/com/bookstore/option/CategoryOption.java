package com.bookstore.option;

import java.io.Serializable;
import java.util.List;

/**
 * @author Administrator
 */
public class CategoryOption implements Serializable {

    private static final long serialVersionUID = 1L;

    public CategoryOption(List<Integer> ids) {
        this.ids = ids;
    }

    public CategoryOption() {
    }

    public CategoryOption(Integer pageStart, Integer pageSize) {
        this.pageStart = pageStart;
        this.pageSize = pageSize;
    }

    private List<Integer> ids;

    private Integer pageStart;

    private Integer pageSize;

    public List<Integer> getIds() {
        return ids;
    }

    public void setIds(List<Integer> ids) {
        this.ids = ids;
    }

    public Integer getPageStart() {
        return pageStart;
    }

    public void setPageStart(Integer pageStart) {
        this.pageStart = pageStart;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
