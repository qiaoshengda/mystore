<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<h1>客户端首页</h1>
<c:choose>
    <c:when test="${sessionScope.user != null }">
        欢迎您，${sessionScope.username }
        <a href="${pageContext.request.contextPath}/my.html" style="margin-left: 10px;">个人中心</a>
        <a href="javascript:exit('${pageContext.request.contextPath}')" style="margin-left: 10px;">退出</a>
    </c:when>
    <c:otherwise>
        <a href="${pageContext.request.contextPath}/login.html">登录</a>
        <a href="${pageContext.request.contextPath}/register.html" style="margin-left: 10px;">注册</a>
    </c:otherwise>
</c:choose>
<h2>管理员登录</h2>
<a href="${pageContext.request.contextPath}/admin/login.html">登录</a>
<h2>导航</h2>
<a href="${pageContext.request.contextPath}/index.html" style="margin-left: 10px;">首页</a>
<a href="${pageContext.request.contextPath}/search.html" style="margin-left: 10px;">搜索</a>