<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="cn.hutool.core.date.DateUtil" %>
<!DOCTYPE html>
<html>
<%@include file="../../layout/head.jsp" %>
<body>
<%@include file="../../layout/header.jsp" %>
<h2>我的导航</h2>
<a href="${pageContext.request.contextPath}/my.html" style="margin-left: 10px;">个人信息</a>
<a href="${pageContext.request.contextPath}/shop.html" style="margin-left: 10px;">购物车</a>
<a href="${pageContext.request.contextPath}/record.html" style="margin-left: 10px;">购买记录</a>
<div>
    <h2>购买记录</h2>
    <c:forEach items="${records}" var="record">
        <div style="margin-top: 40px;border:1px solid #000;width: 400px;padding: 10px">
            <div>
                <img src="${pageContext.request.contextPath}/${record.image}"
                     style="width: 380px;padding: 10px;">
                商品名称：${record.name}
                商品种类：${record.category}
                商品价格：${record.price}</div>
            <div>购买时间：${DateUtil.format(record.createTime, "yyyy-MM-dd HH:mm:ss")}</div>
            <div style="margin-top: 20px;">
                <button type="button"
                        onclick="window.location.href='${pageContext.request.contextPath}/commodity/info/${record.commodityId}.html'"
                        disabled="${record.isEnable == 1 ? '' : 'disabled'}"
                >
                    详情
                </button>
                <c:if test="${record.isEnable == 0}">
                    已下架
                </c:if>
            </div>
        </div>
    </c:forEach>
    <div>
        <c:if test="${pageNum > 1}">
            <a href="${pageContext.request.contextPath}/?pageNum=${pageNum - 1}&pageSize=${pageSize}">上一页</a>
        </c:if>
        <span>当前第${pageNum}页，共${count}页</span>
        <c:if test="${pageNum < count}">
            <a href="${pageContext.request.contextPath}/?pageNum=${pageNum + 1}&pageSize=${pageSize}">下一页</a>
        </c:if>
    </div>
</div>
</body>
</html>