<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<%@include file="../../layout/head.jsp" %>
<body>
<%@include file="../../layout/header.jsp" %>
</body>
<h2>搜索</h2>
<input type="text" value="${name}" id="name" placeholder="请输入商品名称"/>
<button type="button" onclick="search()">搜索</button>
<br>
<input type="text" id="min" value="${min }"/>&nbsp;-&nbsp;
<input type="text" id="max" value="${max }"/>
<h2>结果</h2>
<c:forEach items="${commodities }" var="commodity">
    <div style="margin-top: 40px;border:1px solid #000;width: 400px;padding: 10px">
        <div>
            <img src="${pageContext.request.contextPath}/${commodity.image}" style="width: 380px;padding: 10px;">
            商品名称：${commodity.name}
            商品种类：${commodity.category}
            商品价格：${commodity.price}</div>
        <div style="margin-top: 20px;">
            <button type="button"
                    onclick="window.location.href='${pageContext.request.contextPath}/commodity/info/${commodity.id}.html'">
                详情
            </button>
            <button type="button" onclick="addShop(${commodity.id})">加入购物车</button>
            <button type="button"
                    onclick="window.location.href='${pageContext.request.contextPath}/commodity/pay.html?id=${commodity.id}'">
                购买
            </button>
        </div>
    </div>
</c:forEach>

<div>
    <c:if test="${pageNum > 1}">
        <a href="${pageContext.request.contextPath}/search.html?pageNum=${pageNum - 1}&pageSize=${pageSize}&name=${name}&max=${max}&min=${min}">上一页</a>
    </c:if>
    <span>当前第${pageNum}页，共${count}页</span>
    <c:if test="${pageNum < count}">
        <a href="${pageContext.request.contextPath}/search.html?pageNum=${pageNum + 1}&pageSize=${pageSize}&name=${name}&max=${max}&min=${min}">下一页</a>
    </c:if>
</div>
<script type="text/javascript">

    function addShop(id) {
        $.getJSON('${pageContext.request.contextPath}/commodity/addShop', {id: id}, function (result) {
            if (result.code === 200) {
                alert(result.message);
            } else if (result.code === 401) {
                alert(result.message);
                window.location.href = "${pageContext.request.contextPath}/login.html";
            } else {
                alert(result.message);
            }
        })
    }

    function search() {
        var name = $("#name").val();
        var min = $("#min").val();
        var max = $("#max").val();
        window.location.href = "${pageContext.request.contextPath}/search.html?name=" + name + "&max=" + max + "&min=" + min;
    }

</script>
</html>