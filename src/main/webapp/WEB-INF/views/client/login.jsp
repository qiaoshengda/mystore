<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>登录</title>
    <script type="text/javascript" src="${pageContext.request.contextPath}/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/index.js"></script>
</head>
<body>
<h1>导航</h1>
<a href="${pageContext.request.contextPath}/" style="margin-left: 10px;">首页</a>
<a href="${pageContext.request.contextPath}/search.html" style="margin-left: 10px;">搜索</a>
<h1>登录</h1>
<label for="username">用户名：</label>
<input id="username" type="text" name="username"/><br>
<label for="password">密&nbsp;&nbsp;&nbsp;&nbsp;码：</label>
<input id="password" type="password" name="password"/><br>
<button type="button" id="login-form">登录</button>
<div>
    <span>还没有账号？</span>
    <a href="${pageContext.request.contextPath}/register.html">立即注册</a>
</div>
</body>
<script type="application/javascript">
    $('#login-form').on('click', function () {
        let username = $('#username').val();
        let password = $('#password').val();
        if (!username || username === '') {
            alert('请输入用户名');
            return;
        }
        if (!password || password === '') {
            alert('请输入密码');
            return;
        }
        login('${pageContext.request.contextPath}', username, password);
    })
</script>
</html>