<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>注册</title>
    <script type="text/javascript" src="${pageContext.request.contextPath}/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/index.js"></script>
</head>
<body>
<h1>导航</h1>
<a href="${pageContext.request.contextPath}/" style="margin-left: 10px;">首页</a>
<a href="${pageContext.request.contextPath}/search.html" style="margin-left: 10px;">搜索</a>
<h1>注册</h1>
<label>
    <span>用户名：</span>
    <input type="text" name="username" id="username"/>
</label><br>
<label>
    <span>密码：</span>
    <input type="password" name="password" id="password"/>
</label><br>
<label>
    <span>年龄：</span>
    <input type="text" name="age" id="age"/>
</label><br>
<label>
    <span>性别：</span>
    <input type="radio" name="gender" value="1"/><span>男</span>
    <input type="radio" name="gender" value="0"/><span>女</span>
</label><br>
<button type="button" id="register-form">注册</button>
</body>
<script type="application/javascript">
    $('#register-form').on('click', function () {
        let username = $('#username').val();
        let password = $('#password').val();
        let age = $('#age').val();
        let gender = $('input[name=gender]:checked').val();
        if (username === '') {
            alert('请输入用户名');
            return;
        }
        if (password === '') {
            alert('请输入密码');
            return;
        }
        register('${pageContext.request.contextPath}', {
            username: username,
            password: password,
            age: age,
            gender: gender
        });
    })
</script>
</html>