<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<%@include file="../../layout/head.jsp" %>
<body>
<%@include file="../../layout/header.jsp" %>
<h2>支付</h2>
<h3>商品详情</h3>
<div style="margin-top: 40px;border:1px solid #000;width: 400px;padding: 10px">
    <div>
        <img src="${pageContext.request.contextPath}/${commodity.image}" style="width: 380px;padding: 10px;">
        商品名称：${commodity.name}
        商品种类：${commodity.category}
        商品价格：${commodity.price}
        <c:if test="${commodity.isEnable == 0}">
            <div style="color:gray;">商品已下架</div>
        </c:if>
    </div>
</div>
<h2>准备支付</h2>
当前账户余额：${user.money }<br>
商品价格：${commodity.price}<br>
<button type="button" onclick="buy(${commodity.id})" ${commodity.isEnable == 1 ? "" : "disabled"}>购买</button>
</body>
<c:if test="${commodity.isEnable == 1}">
    <script type="text/javascript">
        function buy(id) {
            $.getJSON('${pageContext.request.contextPath}/commodity/buy', {id: id}, function (result) {
                if (result.code === 200) {
                    alert("购买成功")
                    window.location.href = '${pageContext.request.contextPath}/my.html';
                } else {
                    alert(result.message)
                }
            })
        }
    </script>
</c:if>
</html>