<%@ page contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<%@include file="../../layout/head.jsp"%>
<body>
<%@include file="../../layout/header.jsp"%>
<h2>商品详情</h2>
<div>
	<img src="${pageContext.request.contextPath}/${commodity.image}" /><br />
商品名称：${commodity.name}<br />
商品种类：${commodity.category}<br />
商品价格：${commodity.price}</div>
<div style="margin-top: 20px;">
	<button type="button" onclick="addShop(${commodity.id})" ${commodity.isEnable == 1 ? "" : "disabled"}>加入购物车</button>
	<button type="button" onclick="window.location.href='../pay.html?id=${commodity.id}'" ${commodity.isEnable == 1 ? "" : "disabled"}>购买</button>
</div>
<script type="text/javascript">
function addShop(id){
	$.getJSON('${pageContext.request.contextPath}/commodity/addShop', {id: id}, function(result){
		if(result.code === 200){
			alert(result.message);
		}else if(result.code === 401) {
			alert(result.message);
			window.location.href="${pageContext.request.contextPath}/login.html";
		}else {
			alert(result.message);
		}
	})
}
</script>
</body>
</html>