<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<%@include file="../../layout/head.jsp" %>
<body>
<%@include file="../../layout/header.jsp" %>
<h2>我的导航</h2>
<a href="${pageContext.request.contextPath}/my.html" style="margin-left: 10px;">个人信息</a>
<a href="${pageContext.request.contextPath}/shop.html" style="margin-left: 10px;">购物车</a>
<a href="${pageContext.request.contextPath}/record.html" style="margin-left: 10px;">购买记录</a>
<div style="display: grid;grid-template-rows: 1fr;grid-template-columns: 1fr 1fr 1fr">
    <div>
        <h2>个人信息</h2>
        用户名：<input type="text" id="username" value="${user.username }"><br>
        密码：<input type="password" id="password" value="${user.password }"><br>
        性别：<input type="radio" value="1" name="gender" ${user.gender ? 'checked' : '' }>男
        <input type="radio" value="0" name="gender" ${!user.gender ? 'checked' : '' }>女<br>
        年龄：<input type="text" id="age" value="${user.age}"><br>
        余额：<input type="text" disabled="disabled" value="${user.money}"><br>
        <button type="button" onclick="update()">修改</button>
        <button id="layout" type="button" onclick="layout()">注销</button>
    </div>
</div>
<script type="text/javascript">
    function update() {
        let username = $("#username").val();
        let password = $("#password").val();
        let age = $("#age").val();
        let gender = $("input[name='gender']:checked").val();
        $.getJSON('user/update', {
            username: username,
            password: password,
            age: age,
            gender: gender
        }, function (result) {
            if (result.code === 200) {
                alert("修改成功，请重新登录")
                window.location.href = 'login.html'
            } else {
                alert("修改失败，请重试")
                location.reload()
            }
        })
    }

    function layout() {
        let $layout = $("#layout");
        let string = $layout.text();
        if (string === '注销') {
            $layout.attr('style', 'color: red;')
            $layout.text("确认注销？")
        }
        if (string === '确认注销？') {
            $.getJSON('${pageContext.request.contextPath}/user/layout', function (result) {
                if (result.code === 200) {
                    alert("注销成功");
                    window.location.href = 'index';
                } else {
                    alert("注销失败");
                }
            })
        }
    }
</script>
</body>
</html>