<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<%@include file="../../layout/head.jsp" %>
<body>
<%@include file="../../layout/header.jsp" %>
<h2>我的导航</h2>
<a href="${pageContext.request.contextPath}/my.html" style="margin-left: 10px;">个人信息</a>
<a href="${pageContext.request.contextPath}/shop.html" style="margin-left: 10px;">购物车</a>
<a href="${pageContext.request.contextPath}/record.html" style="margin-left: 10px;">购买记录</a>
<div>
    <h2>购物车</h2>
    <c:forEach items="${shops}" var="shop">
        <div style="margin-top: 40px;border:1px solid #000;width: 400px;padding: 10px">
            <div>
                <img src="${pageContext.request.contextPath}/${shop.image}"
                     style="width: 380px;padding: 10px;">
                商品名称：${shop.name}
                商品种类：${shop.category}
                商品价格：${shop.price}
                <c:if test="${shop.isEnable == 0}">
                    <div style="color:gray;">商品已下架</div>
                </c:if>
            </div>
            <div style="margin-top: 20px;">
                <button type="button"
                        onclick="window.location.href='${pageContext.request.contextPath}/commodity/info/${shop.commodityId}.html'"
                    ${shop.isEnable == 1 ? "" : "disabled"}
                >
                    详情
                </button>
                <button type="button"
                        onclick="removeShop(${shop.id})"
                >
                    移除
                </button>
                <button type="button"
                        onclick="window.location.href='${pageContext.request.contextPath}/commodity/pay.html?id=${shop.commodityId}'"
                    ${shop.isEnable == 1 ? "" : "disabled"}
                >
                    购买
                </button>
            </div>
        </div>
    </c:forEach>
    <div>
        <c:if test="${pageNum > 1}">
            <a href="${pageContext.request.contextPath}/?pageNum=${pageNum - 1}&pageSize=${pageSize}">上一页</a>
        </c:if>
        <span>当前第${pageNum}页，共${count}页</span>
        <c:if test="${pageNum < count}">
            <a href="${pageContext.request.contextPath}/?pageNum=${pageNum + 1}&pageSize=${pageSize}">下一页</a>
        </c:if>
    </div>
</div>
</body>
<script>
    /**
     * 移除购物车
     *
     * @param id 购物车ID
     */
    function removeShop(id) {
        $.getJSON("${pageContext.request.contextPath}/commodity/removeShop?id=" + id, function (res) {
            if (res.code === 200) {
                alert('移除成功');
                location.reload();
            } else {
                alert('移除失败');
            }
        })
    }
</script>
</html>