<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>后台管理</title>
    <script type="text/javascript" src="${pageContext.request.contextPath}/jquery-3.5.1.min.js"></script>
    <style>
        #edit {
            position: fixed;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            background-color: rgba(0, 0, 0, 0.4);
            display: grid;
            grid-template-columns: 1fr 400px 1fr;
            grid-template-rows: 1fr 300px 1fr;
        }

        #dashboard {
            position: relative;
            background-color: rgb(255, 255, 255);
            padding: 40px;
        }

        #close {
            position: absolute;
            top: 20px;
            right: 20px;
            cursor: pointer
        }

        #show_image {
            width: 150px;
        }
    </style>
</head>
<body>
<h1>商品管理</h1>
欢迎您，${sessionScope.admin.username }
<a href="${pageContext.request.contextPath}/admin/exit" style="margin-left: 10px;">退出</a>
<h1>导航</h1>
<a href="${pageContext.request.contextPath}/admin/index.html" style="margin-left: 10px;">首页</a>
<a href="${pageContext.request.contextPath}/admin/user.html" style="margin-left: 10px;">用户管理</a>
<a href="${pageContext.request.contextPath}/admin/commodity.html" style="margin-left: 10px;">商品管理</a>
<a href="${pageContext.request.contextPath}/admin/category.html" style="margin-left: 10px;">分类管理</a>
<h1>商品列表</h1>
<div>
    <button onclick="add_open()">新增</button>
</div>
<c:forEach items="${commodities }" var="commodity">
    <div style="margin-top: 40px;border:1px solid #000;width: 400px;padding: 10px">
        <div>
            <img src="${pageContext.request.contextPath}/${commodity.image}" style="width: 380px;padding: 10px;">
            商品名称：${commodity.name}
            商品种类：${commodity.category}
            商品价格：${commodity.price}</div>
        <div style="margin-top: 20px;">
            <button type="button" onclick="update_open(${commodity.id})">修改</button>
            <button type="button" onclick="enable(${commodity.id}, ${commodity.isEnable})">${commodity.isEnable == 1 ? "下架" : "上架"}</button>
        </div>
    </div>
</c:forEach>
<div id="edit">
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div id="dashboard">
        <div id="close" onclick="edit_close()">关闭</div>
        <form action="${pageContext.request.contextPath}/admin/commodity/add" method="post">
            图片：
            <img src="" id="show_image" alt="图片" style="height: 100px;"><input type="file" id="image"/><br/>
            <label for="name">名称：</label><input type="text" id="name"/><br>
            <label for="category">分类：</label><select id="category">
            <option>请选择</option>
            <c:forEach items="${categories}" var="category">
                <option value="${category.id}">${category.name}</option>
            </c:forEach>
        </select><br>
            <label for="price">价格：</label><input id="price" type="text"/><br>
            <button type="button" id="operation" onclick="add_or_update()">新建</button>
            <input type="reset" value="重置">
        </form>
    </div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
</div>
</body>
<script type="text/javascript">
    let $edit = $("#edit");
    let image_path = "";
    let dialog_status = 0;
    let commodity_id = 0;
    $edit.hide();

    function add_open() {
        // 获取分类
        dialog_status = 1;
        $("#operation").text("新增");
        $edit.show();
    }

    function enable(id, isEnable) {
        let status = isEnable === 0 ? "上架" : "下架"
        $.getJSON('${pageContext.request.contextPath}/admin/commodity/enable/' + id, {}, function (result) {
            if (result.code === 200) {
                alert(status + "成功");
                location.reload();
            } else {
                alert(status + "成功");
            }
        })
    }

    function update_open(id) {
        // 获取产品信息
        $.getJSON('${pageContext.request.contextPath}/admin/commodity/info/' + id, {}, (res) => {
            if (res.code === 200) {
                // 赋值
                let temp = res.data;
                $("#show_image").attr('src', '${pageContext.request.contextPath}/' + temp.image);
                image_path = temp.image;
                $("#name").val(temp.name);
                $("#category").val(temp.categoryId);
                $("#price").val(temp.price);
                dialog_status = 2;
                commodity_id = id;
                $("#operation").text("修改");
                $edit.show();
            } else {
                alert('产品信息获取失败');
                window.location.reload();
            }
        })
    }

    function edit_close() {
        $edit.hide();
        // 清空产品信息
        $("#show_image").attr('src', '');
        $("#name").val('');
        $("#category").val('');
        $("#price").val('');
        image_path = "";
        dialog_status = 0;
    }

    /**
     * 新增或更新
     */
    function add_or_update() {
        if (dialog_status === 1) {
            // 新增
            let name = $("#name").val();
            let category = $("#category").val();
            let price = $("#price").val();
            $.ajax({
                url: '${pageContext.request.contextPath}/admin/commodity/add',
                contentType: "application/json;charset=utf-8",
                data: {
                    image: image_path,
                    name: name,
                    categoryId: category,
                    price: price
                },
                dataType: 'json',
                type: 'POST',
                success: function (result) {
                    if (result.code === 200) {
                        alert("新增成功");
                        window.location.reload();
                    } else {
                        alert("新增失败");
                    }
                }
            })
        } else if (dialog_status === 2) {
            // 更新
            if (commodity_id === 0) {
                alert('系统出错，请刷新后重试')
                edit_close();
                return;
            }
            let name = $("#name").val();
            let category = $("#category").val();
            let price = $("#price").val();
            $.ajax({
                url: '${pageContext.request.contextPath}/admin/commodity/update/' + commodity_id,
                contentType: "application/json;charset=utf-8",
                data: JSON.stringify({
                    image: image_path,
                    name: name,
                    categoryId: category,
                    price: price
                }),
                dataType: 'json',
                type: 'POST',
                success: function (result) {
                    if (result.code === 200) {
                        alert("修改成功");
                        window.location.reload();
                    } else {
                        alert("修改失败");
                    }
                }
            })
        }else {
            alert('系统出错，请刷新后重试')
            edit_close();
        }
    }

    $("#image").on("change", function (e) {
        // 文件上传
        let formData = new FormData();
        formData.append("multipartFile", e.target.files[0]);
        $.ajax({
            url: '${pageContext.request.contextPath}/admin/commodity/upload',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function (res) {
                if (res.code === 200) {
                    image_path = res.data;
                    $("#show_image").attr("src", '${pageContext.request.contextPath}/' + image_path)
                    alert('图片上传成功');
                } else {
                    alert('图片上传失败');
                }
            }
        })
    })
</script>
</html>