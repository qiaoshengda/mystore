<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>后台管理</title>
    <script type="text/javascript" src="${pageContext.request.contextPath}/jquery-3.5.1.min.js"></script>
</head>
<body>
<h1>首页</h1>
欢迎您，${sessionScope.admin.username }
<a href="${pageContext.request.contextPath}/admin/exit" style="margin-left: 10px;">退出</a>
<h1>导航</h1>
<a href="${pageContext.request.contextPath}/admin/index.html" style="margin-left: 10px;">首页</a>
<a href="${pageContext.request.contextPath}/admin/user.html" style="margin-left: 10px;">用户管理</a>
<a href="${pageContext.request.contextPath}/admin/commodity.html" style="margin-left: 10px;">商品管理</a>
<a href="${pageContext.request.contextPath}/admin/category.html" style="margin-left: 10px;">分类管理</a>
<h2>分类管理</h2>
<div>
    <input type="text" name="categoryName" id="categoryName"/>
    <button onclick="add()">新增</button>
</div>
<table border="1">
    <colgroup>
        <col width="100"/>
        <col width="200"/>
        <col/>
    </colgroup>
    <thead>
    <tr>
        <th style="text-align: left">编号</th>
        <th style="text-align: left">名称</th>
        <th style="text-align: left">操作</th>
    </tr>
    </thead>
    <tbody>
    <c:set var="index" value="0"/>
    <c:forEach items="${categories}" var="category">
        <c:set var="index" value="${index + 1}"/>
        <tr>
            <td>${index}</td>
            <td>${category.name}</td>
            <td>
                <button onclick="update(${category.id}, '${category.name}')">修改</button>
                <button onclick="remove(${category.id}, '${category.name}')">删除</button>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div>
    <c:if test="${pageNum > 1}">
        <a href="${pageContext.request.contextPath}/admin/category.html?pageNum=${pageNum - 1}&pageSize=${pageSize}">上一页</a>
    </c:if>
    <span>当前第${pageNum}页，共${count}页</span>
    <c:if test="${pageNum < count}">
        <a href="${pageContext.request.contextPath}/admin/category.html?pageNum=${pageNum + 1}&pageSize=${pageSize}">下一页</a>
    </c:if>
</div>
</body>
<script>
    function add() {
        let category = $("#categoryName").val();
        $.getJSON('${pageContext.request.contextPath}/admin/category/add', {
            name: category
        }, function (result) {
            if (result.code === 200) {
                alert("新增成功");
                location.reload();
            } else {
                alert("新增失败");
            }
        })
    }

    function update(id, name) {
        let newName = prompt("请输入新的名称：", name);
        if (newName) {
            $.ajax({
                url: '${pageContext.request.contextPath}/admin/category/update/' + id,
                contentType: "application/json;charset=utf-8",
                data: JSON.stringify({
                    name: newName
                }),
                dataType: 'json',
                type: 'POST',
                success: function (result) {
                    if (result.code === 200) {
                        alert("修改成功");
                        window.location.reload();
                    } else {
                        alert("修改失败");
                    }
                }
            })
        }
    }

    function remove(id, name) {
        let isRemove = confirm("确认删除：" + name);
        if (isRemove) {
            $.getJSON('${pageContext.request.contextPath}/admin/category/remove/' + id, {}, (res) => {
                if (res.code === 200) {
                    alert("修改成功:" + name);
                }else {
                    alert(res.message)
                }
                location.reload();
            })
        } else {
            alert("取消删除")
        }
    }
</script>
</html>
