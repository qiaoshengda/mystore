function exit(url) {
    $.getJSON(`${url}/user/exit`, function (res) {
        if (res.code === 200) {
            window.location.href = `${url}/`;
        } else {
            alert(res.message)
        }
    })
}

function login(url, username, password) {
    $.getJSON(`${url}/user/login`, {
        username: username,
        password: password
    }, function (res) {
        if (res.code === 200) {
            window.location.href = `${url}/`;
        } else {
            alert(res.message)
        }
    })
}

function register(url, data) {
    $.post(`${url}/user/register`, data, function (res) {
        if (res.code === 200) {
            window.location.href = `${url}/`;
        } else {
            alert(res.message)
        }
    })
}