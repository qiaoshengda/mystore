# my store数据库

create table user
(
    id          int unsigned primary key auto_increment,
    username    varchar(16)      default ''                 not null comment '用户名',
    password    varchar(32)      default ''                 not null comment '密码',
    gender      int unsigned     default 1                  not null comment '性别',
    age         int unsigned     default 0                  not null comment '年龄',
    money       int unsigned     default 0                  not null comment '金额',
    is_enable   tinyint unsigned default 1                  not null comment '是否启用',
    is_delete   tinyint unsigned default 0                  not null comment '逻辑删除',
    create_time datetime         default '1998-08-06 00:00' not null comment '创建时间'
) comment '用户';

create index idx_username_password on user (username, password);

create table category
(
    id   int unsigned auto_increment primary key,
    name varchar(20) default '' not null comment '分类名称'
) comment '分类';

create table commodity
(
    id          int unsigned primary key auto_increment,
    image       varchar(255)     default 'not_found.jpeg'   not null comment '图片',
    name        varchar(10)      default ''                 not null comment '名称',
    category_id int unsigned     default 0                  not null comment '分类ID',
    price       int              default 0                  not null comment '价格',
    is_enable   tinyint unsigned default 1                  not null comment '是否启用',
    is_delete   tinyint unsigned default 0                  not null comment '逻辑删除',
    create_time datetime         default '1998-08-06 00:00' not null comment '创建时间'
) comment '产品';

create table shop
(
    id           int primary key auto_increment,
    user_id      int unsigned default 0 not null comment '用户ID',
    commodity_id int unsigned default 0 not null comment '商品ID',
    constraint fk_sh_us foreign key (user_id) references user (id),
    constraint fk_sh_co foreign key (commodity_id) references commodity (id)
) comment '购物车';

create table record
(
    id           int primary key auto_increment,
    user_id      int unsigned     default 0                  not null comment '用户ID',
    commodity_id int unsigned     default 0                  not null comment '商品ID',
    create_time  datetime         default '1998-08-06 00:00' not null comment '创建时间',
    constraint fk_re_us foreign key (user_id) references user (id),
    constraint fk_re_co foreign key (commodity_id) references commodity (id)
) comment '购买记录';

create table admin
(
    id       int primary key auto_increment,
    username varchar(10) unique default '' not null comment '用户名',
    password varchar(18)        default '' not null comment '密码'
) comment '管理员';

# 数据填充
# 增加分类
insert into category(id, name)
values (1, '日用');
insert into category(id, name)
values (2, '食品');
insert into category(id, name)
values (3, '电器');


# 增加产品
insert into commodity(image, name, category_id, price, create_time)
values ('not_found.jpeg', '洗洁精', 1, 10, '1998-08-06 00:00');
insert into commodity(image, name, category_id, price, create_time)
values ('not_found.jpeg', '大米', 2, 10, '1998-08-06 01:00');
insert into commodity(image, name, category_id, price, create_time)
values ('not_found.jpeg', '电饭锅', 3, 10, '1998-08-06 02:00');

# 增加管理员
insert into admin(username, password)
VALUES ('admin', '123456');