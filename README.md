# 基于SSM的电子商城项目

基于SSM的经典商城项目，通用电子商城模板。

实现了电子商城的基本功能：商品浏览，分类浏览，商品搜索，登录注册，购物车，商品购买，个人中心，购买记录，后台管理功能。

通过简单的修改，就可以变成任何样子的商城类项目。

## 推荐运行环境

1. java8+
2. maven
3. tomcat9

## 安装&运行

1. 新建数据库`mystore`，并导入[database/mysql.sql](database/mysql.sql)文件中建表语句
2. 使用eclipse或者idea导入项目
3. 修改[src/main/resources/db.properties](src/main/resources/db.properties)文件中数据库链接数据
4. 运行，访问<http://localhost:8080/mystore>

## 数据库设计

![数据库图](img/diagram.png)

## 项目截图

- 首页
  ![首页](img/img1.png)
- 登录
  ![登录](img/img2.png)
- 注册
  ![注册](img/img3.png)
- 分类
  ![分类](img/img4.png)

