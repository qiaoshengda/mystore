<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>登录</title>
    <!-- MDUI CSS -->
    <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/mdui@1.0.1/dist/css/mdui.min.css"
            integrity="sha384-cLRrMq39HOZdvE0j6yBojO4+1PrHfB7a9l5qLcmRm/fiWXYY+CndJPmyu5FV/9Tw"
            crossorigin="anonymous"
    />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/main.css">
    <script src="${pageContext.request.contextPath}/jquery-3.5.1.min.js" type="text/javascript"
            charset="utf-8"></script>
    <style>
        .login-main {
            height: 900px;
            display: grid;
            grid-template-rows: 1fr 350px 1fr;
            grid-template-columns: 1fr 600px 1fr;
            background: url("https://esion.xyz/img/bg/%E9%9D%92%E8%9B%87.jpg");
        }

        .login-content {
            background-color: rgba(255, 255, 255, 0.8);
            border-radius: 10px;
        }

        .login-title {
            font-size: 24px;
            font-weight: bold;
            text-align: center;
            height: 30px;
            line-height: 30px;
            margin-bottom: 50px;
            margin-top: 25px;
        }

        .login-item {
            margin: 50px 75px;
        }

        .login-item-content {
            width: 400px;
            display: flex;
        }

        .login-label {
            width: 80px;
            line-height: 36px;
        }

        .login-input {
            width: 100%;
        }
    </style>
</head>
<body>
<div class="header layout">
    <div></div>
    <div class="main header-main">
        <a class="title" href="${pageContext.request.contextPath}/">云落商城首页</a>
        <div class="user">
            <a href="${pageContext.request.contextPath}/login.html">登录</a>
            <a href="${pageContext.request.contextPath}/register.html">注册</a>
        </div>
    </div>
    <div></div>
</div>
<!-- 布局主体 -->
<div class="login-main">
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <form class="login-content">
        <div class="login-title">用户登录</div>
        <div class="login-item">
            <div class="login-item-content">
                <label for="username" class="login-label">用户名：</label>
                <input id="username" class="mdui-textfield-input login-input" type="text" placeholder="用户名" autofocus="autofocus"/>
            </div>
        </div>
        <div class="login-item">
            <div class="login-item-content">
                <label for="password" class="login-label">密&nbsp;&nbsp;&nbsp;&nbsp;码：</label>
                <input id="password"
                       class="mdui-textfield-input login-input"
                       type="password"
                       placeholder="密码"
                />
            </div>
        </div>
        <div class="login-item">
            <div class="login-item-content">
                <label for="password" class="login-label"></label>
                <button type="button" class="mdui-btn color-theme-default mdui-ripple" onclick="login()">登录</button>
                <button type="reset" class="mdui-btn mdui-ripple">重置</button>
            </div>
        </div>
    </form>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
</div>
<div class="footer layout">
    <div></div>
    <div>
        <div class="footer-link">
            <div><a href="https://gitee.com/qiaoshengda/mystore" target="_blank">仓库地址</a></div>
            <div><a href="https://esion.xyz.docs/mystore" target="_blank">说明文档</a></div>
            <div><a href="https://gitee.com/qiaoshengda/mystore/tree/master/theme/default" target="_blank">主题地址</a>
            </div>
            <div><a href="${pageContext.request.contextPath}/admin/login.html" target="_blank">管理员登录</a></div>
        </div>
        <div class="mdui-divider mdui-divider-inset-light"></div>
        <div class="footer-other">
            云落天都版权所有 @ 2021
        </div>
    </div>
    <div></div>
</div>
</body>
</body>
<!-- MDUI JavaScript -->
<script
        src="https://cdn.jsdelivr.net/npm/mdui@1.0.1/dist/js/mdui.min.js"
        integrity="sha384-gCMZcshYKOGRX9r6wbDrvF+TcCCswSHFucUzUPwka+Gr+uHgjlYvkABr95TCOz3A"
        crossorigin="anonymous"
></script>
<script>
    $('#password').on('keydown', (e) => {
        if (e.keyCode === 13) {
            login();
        }
    })
    /**
     * 登录
     */
    function login() {
        let username = $("#username").val();
        let password = $("#password").val();
        if (username === '' || password === '') {
            mdui.alert('用户名或密码为空');
            return;
        }
        $.getJSON('${pageContext.request.contextPath}/user/login', {
            username: username,
            password: password
        }, (res) => {
            if (res.code === 200) {
                location.href = '${pageContext.request.contextPath}/';
            }else if (res.code === 500) {
                mdui.alert(res.message);
            }
        })
    }
</script>
</html>