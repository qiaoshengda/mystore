<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <title>云落商城</title>
    <!-- MDUI CSS -->
    <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/mdui@1.0.1/dist/css/mdui.min.css"
            integrity="sha384-cLRrMq39HOZdvE0j6yBojO4+1PrHfB7a9l5qLcmRm/fiWXYY+CndJPmyu5FV/9Tw"
            crossorigin="anonymous"
    />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/main.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/page.css">
    <script src="${pageContext.request.contextPath}/jquery-3.5.1.min.js" type="text/javascript"
            charset="utf-8"></script>
    <script src="${pageContext.request.contextPath}/assets/jquery.z-pager.js" type="text/javascript"
            charset="utf-8"></script>
    <c:set var="isLogin" value="${sessionScope.user != null}"/>
</head>
<body>
<div class="header layout">
    <div></div>
    <div class="main header-main">
        <a class="title" href="${pageContext.request.contextPath}/">云落商城首页</a>
        <div class="user">
            <c:choose>
                <c:when test="${isLogin}">
                    欢迎您，${sessionScope.username }
                    <a href="${pageContext.request.contextPath}/my.html">个人中心</a>
                    <span onclick="exit()" style="cursor: pointer">退出</span>
                </c:when>
                <c:otherwise>
                    <a href="${pageContext.request.contextPath}/login.html">登录</a>
                    <a href="${pageContext.request.contextPath}/register.html">注册</a>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
    <div></div>
</div>
<div class="top layout">
    <div></div>
    <div class="top-main">
        <div>
            <!-- 可以换为自己的logo图片 -->
            <a class="logo" href="${pageContext.request.contextPath}/">云落商城</a>
        </div>
        <div>
            <div class="search">
                <label>
                    <input class="mdui-textfield-input" type="text" placeholder="商品名称"/>
                </label>
                <button class="mdui-btn mdui-btn-raised mdui-ripple color-theme-default">搜索</button>
            </div>
        </div>
        <div class="shop">
            <div class="shop-main" onclick="window.location.href = '${pageContext.request.contextPath}/shop.html'">
                <i class="mdui-icon material-icons">&#xe8cc;</i>
                <span>购物车</span>
            </div>
        </div>
    </div>
    <div></div>
</div>
<div class="category layout">
    <div></div>
    <div class="category-main">
        <div><a href="${pageContext.request.contextPath}/">首页</a></div>
        <div>
            <c:if test="${categories.size() > 0}">
                <a href="${pageContext.request.contextPath}/category.html?categoryId=${categories.get(0).id}">${categories.get(0).name}</a>
            </c:if>
        </div>
        <div>
            <c:if test="${categories.size() > 1}">
                <a href="${pageContext.request.contextPath}/category.html?categoryId=${categories.get(1).id}">${categories.get(1).name}</a>
            </c:if>
        </div>
        <div>
            <c:if test="${categories.size() > 2}">
                <a href="${pageContext.request.contextPath}/category.html?categoryId=${categories.get(2).id}">${categories.get(2).name}</a>
            </c:if>
        </div>
    </div>
    <div></div>
</div>
<div class="layout" style="margin-top: 50px;">
    <div></div>
    <div class="main">
        <c:forEach items="${commodities}" var="commodity">
            <div class="mdui-card commodity">
                <div class="mdui-card-media">
                    <img class="commodity-image" src="${pageContext.request.contextPath}/${commodity.image}"
                         alt="商品图片"/>
                    <div class="mdui-card-media-covered mdui-card-media-covered-transparent">
                        <div class="mdui-card-primary">
                            <div class="mdui-card-primary-title" style="color: #000000">${commodity.name}</div>
                            <div class="mdui-card-primary-subtitle" style="color: #000000">${commodity.category}
                                <div class="mdui-float-right">价格：${commodity.price}元</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- 卡片的内容 -->
                <div class="mdui-card-actions">
                    <button class="mdui-btn mdui-ripple">详情</button>
                    <button class="mdui-btn mdui-ripple mdui-float-right">加入购物车</button>
                    <button class="mdui-btn mdui-ripple mdui-float-right">购买</button>
                </div>
            </div>
        </c:forEach>
    </div>
    <div></div>
</div>
<div style="text-align: center;margin-top: 10px;">
    <div id="pager" class="pager clearfix">
    </div>
</div>
<div class="footer layout">
    <div></div>
    <div>
        <div class="footer-link">
            <div><a href="https://gitee.com/qiaoshengda/mystore" target="_blank">仓库地址</a></div>
            <div><a href="https://esion.xyz.docs/mystore" target="_blank">说明文档</a></div>
            <div><a href="https://gitee.com/qiaoshengda/mystore/tree/master/theme/default" target="_blank">主题地址</a>
            </div>
            <div><a href="${pageContext.request.contextPath}/admin/login.html" target="_blank">管理员登录</a></div>
        </div>
        <div class="mdui-divider mdui-divider-inset-light"></div>
        <div class="footer-other">
            云落天都版权所有 @ 2021
        </div>
    </div>
    <div></div>
</div>
</body>
<!-- MDUI JavaScript -->
<script
        src="https://cdn.jsdelivr.net/npm/mdui@1.0.1/dist/js/mdui.min.js"
        integrity="sha384-gCMZcshYKOGRX9r6wbDrvF+TcCCswSHFucUzUPwka+Gr+uHgjlYvkABr95TCOz3A"
        crossorigin="anonymous"
></script>
<script>
    $("#pager").zPager({
        totalData: ${total}, //数据总条数
        pageData: ${pageSize}, //每页数据条数
        pageCount: ${count}, //总页数
        current: ${pageNum}, //当前页码数
        pageStep: 8, //当前可见最多页码个数
        minPage: 5, //最小页码数，页码小于此数值则不显示上下分页按钮
        active: 'current', //当前页码样式
        prevBtn: 'pg-prev', //上一页按钮
        nextBtn: 'pg-next', //下一页按钮
        btnBool: true, //是否显示上一页下一页
        firstBtn: 'pg-first', //第一页按钮
        lastBtn: 'pg-last', //最后一页按钮
        btnShow: true, //是否显示第一页和最后一页按钮
        disabled: true, //按钮失效样式
        ajaxSetData: false, //是否使用ajax获取数据 此属性为真时需要url和htmlBox不为空
    });

    function currentPage(currentPage) {
        if (currentPage !== ${pageNum}) {
            window.location.href = '${pageContext.request.contextPath}/?pageNum=' + currentPage + '&pageSize=${pageSize}'
        }
    }

    function exit() {
        $.getJSON('${pageContext.request.contextPath}/user/exit', {}, (res) => {
            if (res.code === 200) {
                mdui.alert('退出成功');
                setTimeout(() => {
                    location.reload();
                }, 2000);
            }
        })
    }
</script>
</html>